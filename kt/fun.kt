import java.util.*

// To compile:
// kotlinc ./kt/<kotlin-file>.kt -include-runtime -d ./jars/<desired-jar-name>.jar

// To run:
// java -jar <jar-name>.jar

/**
 * A sum of two integer values.
 *
 * Function having two Int parameters with Int return type.
 *
 * @param a the first given number.
 * @param b the second given number.
 * @return the result of both given numbers.
*/
fun sum(a: Int, b: Int ): Int {
    return a + b;
}

/**
 * Print of a sum of two integer values.
 *
 * Function with an expression body and inferred return type:
 *
 * @param a the first given number.
 * @param b the second given number.
*/
fun sumWhitoutReturnType(a: Int, b: Int) {
    println("sum of $a and $b is ${sum(a, b)}");
}

/**
 * Print of a sum of two integer values.
 *
 * Function returning no meaningful value::
 *
 * @param a the first given number.
 * @param b the second given number.
*/

/* Dúvidas:
  Qual a diferença entre não retornar nada e retornar um Unit?
*/

fun sumReturningMeaningfulValue(a: Int, b: Int): Unit {
    println("sum of $a and $b is ${a + b}");
}

fun main(args: Array<String>) {
    // println(sum(10,5));
    // sumWhitoutReturnType(10,5);
    sumReturningMeaningfulValue(9,5);
}