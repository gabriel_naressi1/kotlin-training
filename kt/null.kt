fun parseInt(str: String): Int? {
    return str.toIntOrNull()
}

fun printProduct1(arg1: String, arg2: String) {
    val x = parseInt(arg1)
    val y = parseInt(arg2)

    // Using `x * y` yields error because they may hold nulls.    
    if (x != null && y != null) {
        // x and y are automatically cast to non-nullable after null check
        println(x * y)
    } else {
        println("either '$arg1' or '$arg2' is not a number")
    }
}

// OR...

fun printProduct2(arg1: String, arg2: String) {
    val x = parseInt(arg1)
    val y = parseInt(arg2)

    // Using `x * y` yields error because they may hold nulls.    
    if (x == null) {
        println("Wrong number format in arg1: '$arg1'")
        return
    } 
    
    if (y == null) {
        println("Wrong number format in arg2: '$arg2'")
        return
    }

    println(x * y)
}


fun main(args: Array<String>) {
    
    printProduct1("6", "7") //42
    printProduct1("a", "7") // either 'a' or '7' is not a number 
    printProduct1("aa", "b") // either 'aa' or 'b' is not a number 

    printProduct2("6", "7") //42
    printProduct2("a", "7") // Wrong number format in arg1: 'a'
    printProduct2("aa", "b") // Wrong number format in arg1: 'aa'
}