class Shape(val sides: List<Double>) {
  val perimeter: Double get() = sides.sum()
  //abstract fun calculateArea(): Double
}

interface RectangleProperties {
  val isSquare: Boolean
}

// Rectangle implements RectangleProperties and override his method.
class Rectangle (var height: Double, var length: Double) : RectangleProperties {
  override val isSquare: Boolean get() = length == height
  // override fun calculateArea(): Double = height * length
}

// Rectangle2 implements extends Shape and implements RectangleProperties
// class Rectangle2 (var height: Double, var length: Double) : Shape (listOf(height, length)), RectangleProperties {
//   override val isSquare: Boolean get() = length == height
//   // override fun calculateArea(): Double = height * length
// }

fun main(args: Array<String>) {
  val rectangle = Rectangle(5.0, 5.0)
  println(rectangle.isSquare)
}