//Data classes implements toString/equals and hashCode
data class User(val name: String, val age: Int) {}

fun main(args: Array<String>) {
  val user = User("Gabriel", 21)
  val user2 = User("Gabriel", 21)

  println (user) // User(name=Gabriel, age=21)
  println (user.equals(user2)) // true

  //Copying
  println(user.copy("Another name", 33)); //User with different attributes.

  //Destructuring Declarations
  val jane = User("Jane", 35)
  val (name, age) = jane // Like name = jane.name and age = jane.age
  println("$name, $age years of age") // Jane, 35 years of age
}