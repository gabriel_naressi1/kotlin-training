fun main(args: Array<String>) {
    var a = 1

    // simple name in template:
    val s1 = "a is $a"
    println("a = $a") // a = 1

    a = 2
    // arbitrary expression in template:
    val s2 = "${s1.replace("is", "was")}, but now is $a"
    println ("a = $a") // a = 2

    println(s2) //a was 1, but now is 2 
    
    val s = "abc"
    val s3 = "$s.length is ${s.length}" 
    println(s3) // evaluates to "abc.length is 3"

    // escape for '$'
    val price = """
        R${'$'}9.99,00
    """

    println(price) //R$9.99,00 
}