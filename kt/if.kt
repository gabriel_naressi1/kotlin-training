// if as an expression must have both main and 'else' branches
fun maxOf(a: Int, b: Int) = if (a > b) a else b

fun main(args: Array<String>) {
    val a = 0
    val b = 42
    println("The max between $a and $b is ${maxOf(0, 42)}") // The max between 0 and 42 is 42

    //if branches can be blocks, and the last expression is the value of a block:
    val max = if (a > b) {
        print("Choose a: ")
        a
    } else {
        print("Choose b: ")
        b
    }

    println(max) // Choose b: 42
}