/* Dúvidas:
  Qual a diferença val e var?
  R: var pode ser mudado e val não.
*/

//Top-level variables:
val PI = 3.14
var i = 0

fun incrementI() {
  i += 1
}

fun main(args: Array<String>) {
  val a: Int = 1 // immediate assignment

  val b = 2 // `Int` type is inferred

  val c: Int  // Type required when no initializer is provided
  c = 3

  println(a)
  println("B é $b e C é $c")

  var x = 3 // Mutable variable
  x += 4
  println("X é $x")

  incrementI()
  println(i)
}