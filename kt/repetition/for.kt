fun main(args: Array<String>) {
    val items = listOf("apple", "banana", "kiwi")
    for (item in items) {
        //println(item)
    }

    // OR

    for (index in items.indices) {        
        // println(items.indices) // 0..2
        // println(items.size) 3
        // println("item as $index is ${items[index]}")
    }

    //With ranges

    for (x in 1..5) {
        //println(x) // 1 2 3 4 5
    }

    for (x in 1..10 step 2) {
        //println(x) //1 3 5 7 9
    }

    for (x in 9 downTo 0 step 3) {
        println(x) //9 6 3 0
    }
}