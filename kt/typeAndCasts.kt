fun getStringLength(obj: Any): Int? {
     // `obj` is automatically cast to `String` in this branch
    if (obj is String) {
        return obj.length
    }

    // `obj` is still of type `Any` outside of the type-checked branch
    return null
}

// OR

fun getStringLength2(obj: Any): Int? {
    if (obj !is String) return null

    // `obj` is automatically cast to `String` in this branch
    return obj.length
}

// OR

fun getStringLength3(obj: Any): Int? {
    // `obj` is automatically cast to `String` on the right-hand side of `&&`
    if (obj is String && obj.length > 0) {
        return obj.length
    }

    return null
}

// TODO entender melhor como funciona o if ternário.

fun main(args: Array<String>) {
    fun printLength(obj: Any) {
        println("'$obj' string length is ${getStringLength(obj) ?: "... err, not a string"} ")
    }
    
    printLength("Incomprehensibilities") //'Incomprehensibilities' string length is 21 

    printLength(1000) //'1000' string length is ... err, not a string

    printLength(listOf(Any())) //'[java.lang.Object@1d44bcfa]' string length is ... err, not a string      
}