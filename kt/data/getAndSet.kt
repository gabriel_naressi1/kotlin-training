class Address {
  var name: String = "Default address name"
  var street: String = ""
  var number: Int = 0

  // val isEmpty: Boolean
  //   get() = this.number == 0

  get() = field

  set(value) {
    field = if (value < 18) 18 else 21
  }

  // val setName = set(val name : String) {
  //   this.name = name
  // }
}

fun main(args: Array<String>) {
  val address = Address()

  // println ("Default address name is: ${address.name}")

  // address.name = "actual adress name"
  address.number = 22
  // println(address.isEmpty) // true
  // println ("Changed address name is: ${address.name}") // "Adress name"
  println ("Changed number  is: ${address.number}") // 21
}