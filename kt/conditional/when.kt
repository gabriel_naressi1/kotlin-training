fun describe(obj: Any): String = 
when (obj) {
    1           -> "One"
    "Hello"     -> "Greeting"
    is Long     -> "Long"
    !is String  -> "Not a string"
    else        -> "Unknown"
}

fun main(args: Array<String>) {
    println(describe(1)) // "One"
    println(describe("Hello")) // "Greeting"
    println(describe(1000L)) // "Long"
    println(describe(10)) // "Not a string"
    println(describe("whatever")) // Unknown
}   