// class with body
    class Invoice {}


// class without body
    class Empty


// ** Primary constructors:
    class Person constructor(fisrtName: String) {}

    /**
        If the primary constructor does not have any annotations or 
        visibility modifiers, the constructor keyword can be omitted:

        Primary constructor whitout keyword constructor:
    **/
    class Person2(fisrtName: String) {}

    /** 
        If the constructor has annotations or visibility modifiers, 
        the constructor keyword is required, and the modifiers go before it:
    **/
    //class Customer public @Inject constructor(name: String) {}

    // Declaring properties and initializing them from the primary constructor
    class Person3(val firstName: String, val lastName: String, var age: Int) {}

// End of primary constructors


// *** Secondary constructors:

    class Person4(val name: String) {
        constructor(name: String, parent: Person4) : this(name) {
            parent.children.add(this)
        }
    }

// End of secondary constructors


/**
    During an instance initialization, the initializer blocks are executed 
    in the same order as they appear in the class body
**/
class InitOrderDemo(name: String) {
    val customerKey = name.toUpperCase()
    val firstProperty = "First property: $name".also(::println)

    init {
        println("First initializer block that prints ${name}")
    }

    val secondProperty = "Second property: ${name.length}".also(::println)

    init {
        println("Second initializer block that prints ${name.length}")
    }
}

fun main(args: Array<String>) {
        
    InitOrderDemo("hello")

    
}