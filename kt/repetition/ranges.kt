fun main (args: Array<String>) {
    val x = 10
    val y = 9
    if (x in 1..y+1) {
        println("fits in range")
    }


    // Check if a number is out of range:

    val list = listOf("a", "b", "c")

    if (-1 !in 0..list.lastIndex) {
        println("0..${list.lastIndex}") // 0..2
        println("-1 is out of range")
    }

    if (list.size !in list.indices) {
        println("list size is out of valid list indices range too")
    }
}