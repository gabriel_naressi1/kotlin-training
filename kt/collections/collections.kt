fun main (args: Array<String>) {
    //Items is a collection
    val itemsList = listOf("apple", "banana", "kiwi")
    for (item in itemsList) {
        //println(item) //apple banana kiwi
    }

    val itemsSet = setOf("apple", "banana", "kiwi")

    when {
        // "orange" in itemsSet -> println("juicy")
        // "apple" in itemsSet  -> println("pie")
    }

    // Lambda expressions
    val fruits = listOf("banana", "avocado", "apple", "kiwi")
    fruits
    .filter { it.startsWith("a") }
    .sortedBy { it }
    .map { it.toUpperCase() }
    .forEach { println (it) }
}